from keras import layers
from keras import models
from keras.utils import to_categorical
from keras.applications import VGG16
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import preprocessing


def load_data():
    path = '/Users/fbrubacher/Downloads/'
    train = pd.read_csv(path + 'train.csv')
    pred = pd.read_csv(path + 'test.csv')
    return train, pred


def model(model):
    # model = models.Sequential()
    # model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(28, 28, 1)))
    # model.add(layers.MaxPooling2D((2, 2)))
    # model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    # model.add(layers.MaxPooling2D((2, 2)))
    # model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.Flatten())
    model.add(layers.Dense(64, activation='relu'))
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(10, activation='softmax'))
    return model

def reshape(train, pred):
    train_images = train.iloc[:, 1:].values
    train_images = train_images.astype(np.float) / 255
    # train_images = np.multiply(train_images, 1.0 / 255.0)
    train_labels = to_categorical(train['label'])
    X_train, X_test, y_train, y_test = train_test_split(train_images, train_labels, test_size=0.10, random_state=42)
    X_train = X_train.reshape((X_train.shape[0], 28, 28, 1))
    X_test = X_test.reshape((X_test.shape[0], 28, 28, 1))

    pred_images = pred.astype(np.float) / 255
    pred_images = pred_images.as_matrix()
    pred_images = pred_images.reshape((pred_images.shape[0], 28, 28, 1))
    return X_train, X_test, y_train, y_test, pred_images

init = models.Sequential()
conv_base = VGG16(weights='imagenet', include_top=False, input_shape=(28, 28, 1))
init.add(conv_base)
model = model(init)
train, pred = load_data()
X_train, X_test, y_train, y_test, pred_images = reshape(train, pred)


model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
model.fit(X_train, y_train, epochs=5, batch_size=64)

# test_loss, test_acc = model.evaluate(X_test, y_test)
# print(test_acc)
prediction = model.predict_classes(pred_images)
with open("predictions", 'w') as file:
    file.write("ImageId,Label\n")
    for i in range(len(prediction)):
        file.write(str(i + 1) + "," + str(prediction[i]) + "\n")
